setTimeout(function() {
    $(document).ready(function() {

        $(".se-pre-con").fadeOut("slow");

        var inverted = false;
        $("#theme").click(function() {
            if (!inverted) {
                $("html").addClass('inverted');
                $("body").addClass('bg-black');
                inverted = true;
            } else {
                $("html").removeClass('inverted');
                $("body").removeClass('bg-black');
                inverted = false;
            }
        });

        $("#accordion").on('click','.accordion-heading', function (e) {
            if ($(this).hasClass('active')) {
                $(this).next('div').slideUp();
                $(this).removeClass('active');
            } else {
                $("#accordion .panel2").slideUp();
                $("#accordion .accordion-heading").removeClass('active');
                $(this).next('div').stop(true,false).slideDown();
                $(this).addClass('active');
            }
        });
    });
}, 3000);