from django import forms

class CreateStatus(forms.Form):
    status = forms.CharField(label="Status", required=True, max_length=300, widget=forms.TextInput(attrs={'class': 'form-control'}))