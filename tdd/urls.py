from django.conf.urls import url
from .views import index, statuses, profile

urlpatterns = [
    url('add_status/', index),
    url('profile', profile),
    url('', statuses, name='index')
]