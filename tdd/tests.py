from django.test import TestCase, Client
from django.urls import resolve
from selenium.webdriver.chrome.options import Options

from .views import index, statuses, profile
from .models import Status
from .forms import CreateStatus
from selenium import webdriver
import unittest

# Create your tests here.


class UnitTest(TestCase):

    def test_tdd_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_tdd_using_statuses_func(self):
        found = resolve('/')
        self.assertEqual(found.func, statuses)

    def test_tdd_has_hello_world(self):
        response = Client().get("/")
        hello = response.content.decode("utf-8")
        self.assertIn("Hello, Apa Kabar?", hello)

    def test_model_can_create_new_status(self):
        status = Status.objects.create(status="sip")
        total_statuses = Status.objects.all().count()
        self.assertEqual(total_statuses, 1)

    def test_form_valid_for_blank_entries(self):
        form = CreateStatus(data={'status': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['status'], ["This field is required."])

    def test_status_post_success_and_render_result(self):
        test = 'Anonymous'
        response_post = Client().post('/add_status/', {'status': test})
        self.assertEqual(response_post.status_code, 302)
        response = Client().get('/')
        html_response = response.content.decode('utf-8')
        self.assertIn(test, html_response)

    def test_status_post_error_and_render_result(self):
        test = 'Anonymous'
        response_post = Client().post('/add_status/', {'status': ''})
        self.assertEqual(response_post.status_code, 302)
        response = Client().get('/')
        html_response = response.content.decode('utf-8')
        self.assertNotIn(test, html_response)

    def test_form_get_and_render_result(self):
        test = 'Anonymous'
        response_post = Client().get('/add_status/')
        self.assertEqual(response_post.status_code, 200)

    def test_self_func(self):
        status_message = 'HELLO'
        Status.objects.create(status=status_message)
        status = Status.objects.get(id=1)
        self.assertEqual(status_message, status.status)

    def test_profile_url_is_exist(self):
        response = Client().get('/profile')
        self.assertEqual(response.status_code, 200)

    def test_profile_using_profile_func(self):
        found = resolve('/profile')
        self.assertEqual(found.func, profile)

    def test_profile_has_hello(self):
        response = Client().get('/profile')
        hello = response.content.decode('utf-8')
        self.assertIn("Halo, nama saya Farril Zavier Fernaldy.", hello)


# class FunctionalTest(unittest.TestCase):
#
#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')
#         self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         super(FunctionalTest, self).setUp()
#
#     def tearDown(self):
#         self.selenium.implicitly_wait(3)
#         self.selenium.quit()
#
#     def test_can_post_and_see_results(self):
#         self.selenium.get('https://ppw-tdd-farril-zavier.herokuapp.com/')
#         input_box = self.selenium.find_element_by_name('status')
#         input_box.send_keys('Coba Coba')
#         input_box.submit()
#         self.assertIn("Coba Coba", self.selenium.page_source)
#
#     def test_header_property_background_color(self):
#         self.selenium.get('https://ppw-tdd-farril-zavier.herokuapp.com/')
#         navbar = self.selenium.find_element_by_tag_name('nav')
#         self.assertEqual('rgba(121, 198, 200, 1)', navbar.value_of_css_property('background-color'))
#
#     def test_header_property_display(self):
#         self.selenium.get('https://ppw-tdd-farril-zavier.herokuapp.com/')
#         navbar = self.selenium.find_element_by_tag_name('nav')
#         self.assertEqual('flex', navbar.value_of_css_property('display'))
#
#     def test_header_position(self):
#         self.selenium.get('https://ppw-tdd-farril-zavier.herokuapp.com/')
#         navbar = self.selenium.find_element_by_tag_name('nav')
#         self.assertEqual({'x': 0, 'y': 0}, navbar.location)
#
#     def test_submit_button_position(self):
#         self.selenium.get('https://ppw-tdd-farril-zavier.herokuapp.com/')
#         submit_button = self.selenium.find_element_by_tag_name('button')
#         self.assertEqual({'x': 728, 'y': 14}, submit_button.location)
