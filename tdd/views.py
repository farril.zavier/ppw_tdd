from django.http import HttpResponseRedirect
from django.shortcuts import render
from .models import Status
from .forms import CreateStatus

# Create your views here.
response = {}

def index(request):
    response['navItem'] = 'index'
    if request.method == 'POST':
        form = CreateStatus(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            Status.objects.create(**data)
            return HttpResponseRedirect('/')
        else:
            return HttpResponseRedirect('/')
    else:
        form = CreateStatus()
    return render(request, 'index.html', response)

def statuses(request):
    response['navItem'] = 'index'
    all_statuses = Status.objects.all()
    response['status'] = all_statuses
    response['form'] = CreateStatus
    return render(request, 'index.html', response)

def profile(request):
    response['navItem'] = 'profile'
    return render(request, 'profile.html', response)