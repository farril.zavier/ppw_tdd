from datetime import datetime
from django.db import models

# Create your models here.
class Status(models.Model):
    status = models.CharField(max_length=300)
    date = models.DateTimeField(default=datetime.now)
